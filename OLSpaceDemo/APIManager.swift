//
//  APIManager.swift
//  OLSpaceDemo
//
//  Created by Andreea Lavinia Ionescu on 19/10/2019.
//  Copyright © 2019 OLS. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire

let BASE_URL = "https://www.n2yo.com/rest/v1/"
let APIKeyURL = "/?apiKey=XNVUFW-2MU23B-U9VFQQ-47LS"

let SattelitesURL = "satellite/above/%@/%@/%@/15/0"

class APIManager {
    /**
     Request: https://www.n2yo.com/rest/v1 /above/{observer_lat}/{observer_lng}/{observer_alt}/{search_radius}/{category_id}
     */
    func getNearbySattelites(deviceLocation: CLLocation, result: @escaping ([Sattelite]?, Error?) -> Void) {
        let coordinate = deviceLocation.coordinate
        
        let params = String(format: SattelitesURL, String(coordinate.latitude), String(coordinate.longitude), String(deviceLocation.altitude))
        let url = URL(string: BASE_URL + params + APIKeyURL)!
                
         Alamofire.request(
                    url,
                    method: .get,
                    parameters: nil)
                    .validate()
                    .responseJSON { (response) -> Void in
                        guard response.result.isSuccess else {
                            result(nil, response.result.error)
                            return
                        }
                        guard let value = response.result.value as? [String: Any],
                            let satellites = value["above"] as? [[String: Any]] else {
                                print("Malformed data received from fetchAllNews service")
                                result(nil, nil)
                                return
                        }
                        let allSatts = satellites.compactMap({ (satelliteDictionary) -> Sattelite? in
                            return Sattelite(withData: satelliteDictionary)
                        })
                        result(allSatts, nil)
                }
    }
}
