//
//  ViewController.swift
//  OLSpaceDemo
//
//  Created by Andreea Lavinia Ionescu on 19/10/2019.
//  Copyright © 2019 OLS. All rights reserved.
//

import UIKit
import RealityKit
import ARCL
import ARKit
import MapKit
import SceneKit
import Photos
import ImageIO
import MobileCoreServices


class ARSattelitesViewController: UIViewController {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var takePicture: UIButton!

    var viewModel: ARSattelitesViewModel = ARSattelitesViewModel()
    var locationModel: LocationManager = LocationManager()

    let sceneLocationView = SceneLocationView()
    
    var userAnnotation: MKPointAnnotation?
    var locationEstimateAnnotation: MKPointAnnotation?
    
    var updateUserLocationTimer: Timer?
    var updateSatteliteTimer: Timer?
    
    var centerMapOnUserLocation: Bool = true
    var routes: [MKRoute]?
    
    var showMap = false {
        didSet {
            guard let mapView = mapView else {
                return
            }
            mapView.isHidden = !showMap
        }
    }
    
    /// Whether to display some debugging data
    /// This currently displays the coordinate of the best location estimate
    /// The initial value is respected
    let displayDebugging = false
    
    let adjustNorthByTappingSidesOfScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add the box anchor to the scene
        locationModel.requestAuthorization()
        getSattelites()
        
        updateSatteliteTimer = Timer.scheduledTimer(timeInterval: 5.0,
                                                    target: self,
                                                    selector: #selector(ARSattelitesViewController.getSattelites),
                                                    userInfo: nil,
                                                    repeats: true)
        
        // Set to true to display an arrow which points north.
        // Checkout the comments in the property description and on the readme on this.
//        sceneLocationView.orientToTrueNorth = false
//        sceneLocationView.locationEstimateMethod = .coreLocationDataOnly
        
        sceneLocationView.showAxesNode = true
        sceneLocationView.showFeaturePoints = displayDebugging
        
//        sceneLocationView.delegate = self as! ARSCNViewDelegate // Causes an assertionFailure - use the `arViewDelegate` instead:
        sceneLocationView.locationViewDelegate = self as? SceneLocationViewDelegate
        
        // Now add the route or location annotations as appropriate
//        addSceneModels()
        
        contentView.addSubview(sceneLocationView)
        sceneLocationView.frame = contentView.bounds
        
        mapView.isHidden = !showMap
        
        if showMap {
            updateUserLocationTimer = Timer.scheduledTimer(
                timeInterval: 0.5,
                target: self,
                selector: #selector(ARSattelitesViewController.updateUserLocation),
                userInfo: nil,
                repeats: true)
            
            routes?.forEach { mapView.addOverlay($0.polyline) }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        sceneLocationView.run()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print("pause")
        // Pause the view's session
        sceneLocationView.pause()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sceneLocationView.frame = contentView.bounds
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesBegan(touches, with: event)
//        guard let touch = touches.first,
//            let view = touch.view else { return }
//
//        if mapView == view || mapView.recursiveSubviews().contains(view) {
//            centerMapOnUserLocation = false
//        } else {
//            let location = touch.location(in: self.view)
//
//            if location.x <= 40 && adjustNorthByTappingSidesOfScreen {
//                print("left side of the screen")
//                sceneLocationView.moveSceneHeadingAntiClockwise()
//            } else if location.x >= view.frame.size.width - 40 && adjustNorthByTappingSidesOfScreen {
//                print("right side of the screen")
//                sceneLocationView.moveSceneHeadingClockwise()
//            } else {
//                /*
//                let image = UIImage(named: "pin")!
//                let annotationNode = LocationAnnotationNode(location: nil, image: image)
//                annotationNode.scaleRelativeToDistance = false
//                annotationNode.scalingScheme = .normal
//                sceneLocationView.addLocationNodeForCurrentPosition(locationNode: annotationNode)
//                */
//            }
//        }
    }
}

// MARK: - MKMapViewDelegate

extension ARSattelitesViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.lineWidth = 3
        renderer.strokeColor = UIColor.blue.withAlphaComponent(0.5)
        
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation),
            let pointAnnotation = annotation as? MKPointAnnotation else { return nil }
        
        let marker = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: nil)
        
        if pointAnnotation == self.userAnnotation {
            marker.displayPriority = .required
            marker.glyphImage = UIImage(named: "user")
        } else {
            marker.displayPriority = .required
            marker.markerTintColor = UIColor(hue: 0.267, saturation: 0.67, brightness: 0.77, alpha: 1.0)
            marker.glyphImage = UIImage(named: "compass")
        }
        
        return marker
    }
    
    @IBAction func takePicture(_ sender: UIButton?) {
        let picture = contentView.takeScreenshot()
        
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAsset(from: picture)
        }, completionHandler: { success, error in
            if success {
                DispatchQueue.main.async {
                    // save it into documents
                    let fileName = UUID().uuidString
                    if self.saveImage(image: picture, name: fileName) {
                        let imageURL = self.getSavedImage(fileName)
                        
                        var filesToShare = [Any]()
                        guard let url = imageURL else {
                            return
                        }
                        filesToShare.append(url)
                        let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
                        self.present(activityViewController, animated: true) { }
                    }
                }
            }
            else if let error = error {
                // Save photo failed with error
            }
            else {
                // Save photo failed with no error
            }
        })
    }
    
    // MARK: - Save images to Documents
    
    func saveImage(image: UIImage, name: String) -> Bool {
        guard let data = image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent(name + ".png")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    func getSavedImage(_ named: String) -> URL? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            let url = URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named + ".png")
//            let image = UIImage(contentsOfFile: url)
            return url
        }
        return nil
    }
}

// MARK: - Implementation

extension ARSattelitesViewController {
    
    /// Adds the appropriate ARKit models to the scene.  Note: that this won't
    /// do anything until the scene has a `currentLocation`.  It "polls" on that
    /// and when a location is finally discovered, the models are added.
    func addSceneModels() {
        // 1. Don't try to add the models to the scene until we have a current location
        guard sceneLocationView.sceneLocationManager.currentLocation != nil else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                self?.addSceneModels()
            }
            return
        }
        
        let box = SCNBox(width: 1, height: 0.2, length: 5, chamferRadius: 0.25)
        box.firstMaterial?.diffuse.contents = UIColor.gray.withAlphaComponent(0.5)
        
        // 2. If there is a route, show that
        if let routes = routes {
            sceneLocationView.addRoutes(routes: routes) { distance -> SCNBox in
                let box = SCNBox(width: 1.75, height: 0.5, length: distance, chamferRadius: 0.25)
                
                //                // Option 1: An absolutely terrible box material set (that demonstrates what you can do):
                //                box.materials = ["box0", "box1", "box2", "box3", "box4", "box5"].map {
                //                    let material = SCNMaterial()
                //                    material.diffuse.contents = UIImage(named: $0)
                //                    return material
                //                }
                
                // Option 2: Something more typical
                box.firstMaterial?.diffuse.contents = UIColor.blue.withAlphaComponent(0.7)
                return box
            }
        } else {
            // 3. If not, then show the
            buildSattData().forEach {
                sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: $0)
            }
        }
    }
    
    /// Builds the location annotations for a few random objects, scattered across the country
    ///
    /// - Returns: an array of annotation nodes.
    func buildSattData() -> [LocationAnnotationNode] {
        //clear current sattelites from the sky
        sceneLocationView.removeAllNodes()
        
        var nodes: [LocationAnnotationNode] = []
        guard let sattelitesList = viewModel.sattelites else {
            // Fake sattelite
            let djingoTeam = buildNode(latitude: 26.105396685953906, longitude: 44.47905763130687, altitude: 10000, imageName: "sat1", titleVal: "Test SAT")
            nodes.append(djingoTeam)
            return nodes
        }
        for sattelite in sattelitesList {
            let satObject = buildNode(latitude: sattelite.satlat, longitude: sattelite.satlng, altitude: sattelite.satalt * 50, imageName: "sat1", titleVal: sattelite.satname)
            nodes.append(satObject)
        }
                
        return nodes
    }
    
    @objc func getSattelites() {
        guard let location = locationModel.currentLocation else {
            return
        }
        
        viewModel.getSattelites(location: location) { (satellites, error) in
            // refresh the sattelite list
            self.addSceneModels()
        }
    }
    
    @objc func updateUserLocation() {
        guard let currentLocation = sceneLocationView.sceneLocationManager.currentLocation else {
            return
        }
        
        DispatchQueue.main.async { [weak self ] in
            guard let self = self else {
                return
            }
            
            if self.userAnnotation == nil {
                self.userAnnotation = MKPointAnnotation()
                self.mapView.addAnnotation(self.userAnnotation!)
            }
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .allowUserInteraction, animations: {
                self.userAnnotation?.coordinate = currentLocation.coordinate
            }, completion: nil)
            
            if self.centerMapOnUserLocation {
                UIView.animate(withDuration: 0.45,
                               delay: 0,
                               options: .allowUserInteraction,
                               animations: {
                                self.mapView.setCenter(self.userAnnotation!.coordinate, animated: false)
                }, completion: { _ in
                    self.mapView.region.span = MKCoordinateSpan(latitudeDelta: 0.0005, longitudeDelta: 0.0005)
                })
            }
            
            if self.displayDebugging {
                if let bestLocationEstimate = self.sceneLocationView.sceneLocationManager.bestLocationEstimate {
                    if self.locationEstimateAnnotation == nil {
                        self.locationEstimateAnnotation = MKPointAnnotation()
                        self.mapView.addAnnotation(self.locationEstimateAnnotation!)
                    }
                    self.locationEstimateAnnotation?.coordinate = bestLocationEstimate.location.coordinate
                } else if self.locationEstimateAnnotation != nil {
                    self.mapView.removeAnnotation(self.locationEstimateAnnotation!)
                    self.locationEstimateAnnotation = nil
                }
            }
        }
    }
    
    func buildNode(latitude: CLLocationDegrees, longitude: CLLocationDegrees,
                   altitude: CLLocationDistance, imageName: String, titleVal: String) -> LocationAnnotationNode {
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let location = CLLocation(coordinate: coordinate, altitude: altitude)
        let image = UIImage(named: imageName)!
        let node = LocationAnnotationNode(location: location, image: image)
        
        return node
    }
    
    func buildViewNode(latitude: CLLocationDegrees, longitude: CLLocationDegrees,
                       altitude: CLLocationDistance, title: String) -> LocationAnnotationNode {
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let location = CLLocation(coordinate: coordinate, altitude: altitude)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        label.text = title
        label.backgroundColor = .green
        label.textAlignment = .center
        return LocationAnnotationNode(location: location, view: label)
    }
}

// MARK: - Helpers

extension DispatchQueue {
    func asyncAfter(timeInterval: TimeInterval, execute: @escaping () -> Void) {
        self.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(timeInterval * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
            execute: execute)
    }
}
