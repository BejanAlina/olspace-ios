//
//  ARSattelitesViewModel.swift
//  OLSpaceDemo
//
//  Created by Andreea Lavinia Ionescu on 19/10/2019.
//  Copyright © 2019 OLS. All rights reserved.
//

import UIKit
import CoreLocation

class ARSattelitesViewModel: NSObject {

    var apiManager = APIManager()
    var sattelites: [Sattelite]?
    
    func getSattelites(location: CLLocation, result: @escaping ([Sattelite]?, Error?) -> Void) {
        sattelites?.removeAll()
        apiManager.getNearbySattelites(deviceLocation: location) { (sattelitesResult, error) in
            self.sattelites = sattelitesResult
            result(sattelitesResult, error)
        }
    }
}
