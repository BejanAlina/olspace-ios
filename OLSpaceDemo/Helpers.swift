//
//  Helpers.swift
//  OLSpaceDemo
//
//  Created by Andreea Lavinia Ionescu on 20/10/2019.
//  Copyright © 2019 OLS. All rights reserved.
//

import UIKit


extension UIView {
    func takeScreenshot() -> UIImage {
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let img = image {
            return img
        }
        return UIImage()
    }
}
