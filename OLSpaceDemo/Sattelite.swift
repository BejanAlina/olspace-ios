//
//  Sattelite.swift
//  OLSpaceDemo
//
//  Created by Andreea Lavinia Ionescu on 19/10/2019.
//  Copyright © 2019 OLS. All rights reserved.
//

import Foundation

public typealias JSONType = [String:Any]

public struct SatteliteCodingKeys {
    static let satid = "satid"
    static let satname = "satname"
    static let intDesignator = "intDesignator"
    static let launchDate = "launchDate"
    static let satlat = "satlat"
    static let satlng = "satlng"
    static let satalt = "satalt"
}

class Sattelite {
    var satid: Int //20480,
    var satname: String //"JAS 1B (FUJI 2)",
    var intDesignator: String? //"1990-013C",
    var launchDate: String? //"1990-02-07",
    var satlat: Double //49.5744,
    var satlng: Double //-96.7081,
    var satalt: Double //1227.9326
        
    required public init() {
        fatalError()
    }
    
    required public init?(withData data: JSONType) {
        // required values
        guard let satid = data[SatteliteCodingKeys.satid] as? Int,
              let satname = data[SatteliteCodingKeys.satname] as? String,
              let satlat = data[SatteliteCodingKeys.satlat] as? Double,
              let satlng = data[SatteliteCodingKeys.satlng] as? Double,
              let satalt = data[SatteliteCodingKeys.satalt] as? Double else {
                return nil
        }
        self.satid = satid
        self.satname = satname
        self.satlat = satlat
        self.satlng = satlng
        self.satalt = satalt
        
        self.intDesignator = data[SatteliteCodingKeys.intDesignator] as? String
        self.launchDate = data[SatteliteCodingKeys.launchDate] as? String
    }
    
}


